module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: 'react-app',
  plugins: ['import', 'jsx-a11y', 'prettier', 'react', 'react-hooks'],
  rules: {
    'prettier/prettier': 'error'
  }
}
