import React, { useState, useEffect } from 'react'
import { Menu } from 'antd'

import { titleCase } from '../methods'

export default ({ onSelect, isActive, data }) => {
  const [top, setTop] = useState('anime')

  const handleSelect = (value) => {
    setTop(value.key)
    onSelect(value.key)
  }

  useEffect(() => {
    const value = isActive === 'top' ? top : ''
    setTop(value)
  }, [isActive, top])

  return (
    <Menu
      mode="horizontal"
      theme="dark"
      style={{ display: 'flex', justifyContent: 'center' }}
      onClick={handleSelect}
      selectedKeys={[top]}
    >
      {data.map(({ top }) => (
        <Menu.Item key={top}>{titleCase(top)}</Menu.Item>
      ))}
    </Menu>
  )
}
