import React, { useState } from 'react'
import { Pagination } from 'antd'

export default ({ onSelect, total }) => {
  const [page, setPage] = useState(1)

  const handleSelect = (value) => {
    setPage(value)
    onSelect(value)
  }

  return (
    <Pagination
      current={page}
      total={total ? total * 10 : 0}
      disabled={!total}
      showSizeChanger={false}
      onChange={handleSelect}
      style={{ padding: '25px 0' }}
    />
  )
}
