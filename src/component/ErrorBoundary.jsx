import React, { Component } from 'react'
import { Typography } from 'antd'

const { Title } = Typography

export default class ErrorBoundary extends Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true })
    console.log(error, info)
  }

  render() {
    if (this.state.hasError) {
      return (
        <div style={{ textAlign: 'center' }}>
          <Title level={1}>An Error Occured</Title>
        </div>
      )
    }
    return this.props.children
  }
}
