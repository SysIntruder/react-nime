import React from 'react'
import { Row, Col, Card, Typography } from 'antd'
import { titleCase } from '../methods'

const { Text, Title } = Typography

const generateText = (label, data) => {
  if (data) {
    return (
      <div>
        <Text strong>{label}: </Text>
        {data}
      </div>
    )
  }
}

export default ({ data, mode, type }) => {
  if (data && data.length < 0) {
    return <span>No Data</span>
  }
  return (
    <Row gutter={[16, 16]}>
      <Col span={24} style={{ textAlign: 'center' }}>
        <Title level={2}>
          {titleCase(mode === 'search' ? 'result' : 'top')}{' '}
          {titleCase(type)}
        </Title>
      </Col>
      {data.map((item, index) => {
        return (
          <Col sm={24} md={8} key={index}>
            <Card hoverable className="info-card">
              <Row>
                <Col className="image-responsive" md={24} lg={8}>
                  <img alt={item.title} src={item.image_url} />
                </Col>
                <Col style={{ padding: '16px' }} md={24} lg={16}>
                  {generateText('Rank', item.rank)}
                  {generateText('Name', item.name)}
                  {generateText(
                    item.name_kanji ? 'Name' : 'Title',
                    item.title
                  )}
                  {generateText('Name Kanji', item.name_kanji)}
                  {generateText('Type', item.type)}
                  {generateText('Episodes', item.episodes)}
                  {generateText('Volumes', item.volumes)}
                  {generateText('Score', item.score)}
                  {generateText('Favorites', item.favorites)}
                </Col>
              </Row>
            </Card>
          </Col>
        )
      })}
    </Row>
  )
}
