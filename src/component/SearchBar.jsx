import React, { useState } from 'react'
import { Input, Select } from 'antd'

import { titleCase } from '../methods'

const { Option } = Select
const { Search } = Input

export default ({ onSearch, onSelect, data }) => {
  const [search, setSearch] = useState({
    type: 'anime',
    query: null
  })

  const handleSearch = (value) => {
    const obj = { ...search, query: value }
    setSearch(obj)
    onSearch(obj)
  }

  const handleSelect = (value) => {
    const obj = { type: value, query: null }
    setSearch(obj)
    onSelect(obj)
  }

  return (
    <Search
      id="search-bar"
      size="large"
      addonBefore={
        <Select defaultValue="anime" onChange={handleSelect}>
          {data.map(({ search }, index) => (
            <Option value={search} key={index}>
              {titleCase(search)}
            </Option>
          ))}
        </Select>
      }
      placeholder="Input search text"
      enterButton="Search"
      onSearch={handleSearch}
    />
  )
}
