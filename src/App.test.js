import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

test('renders layout', () => {
  const { getByText } = render(<App />)
  const title = getByText(/Title/i)
  expect(title).toBeInTheDocument()
})
