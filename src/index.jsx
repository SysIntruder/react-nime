import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'
import ErrorBoundary from './component/ErrorBoundary'
import * as serviceWorker from './serviceWorker'

import './index.css'

ReactDOM.render(
  <React.StrictMode>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
  </React.StrictMode>,
  document.getElementById('root')
)

serviceWorker.unregister()
