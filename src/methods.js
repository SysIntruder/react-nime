import { useState, useEffect } from 'react'

export function titleCase(str) {
  return str
    .toLowerCase()
    .split(' ')
    .map((word) => {
      return word.charAt(0).toUpperCase() + word.slice(1)
    })
    .join(' ')
}

export function useFetch(url) {
  const [apiReq, setApiReq] = useState({
    data: null,
    total: 0,
    loading: true,
    error: false
  })

  const getDataFromApi = async (url) => {
    try {
      setApiReq({ loading: true })
      const response = await fetch(url)

      if (!response.ok) {
        throw Error(response.statusText)
      }

      const json = await response.json()
      const data = json.top ? json.top : json.results

      setApiReq({
        data,
        total: json.last_page ? json.last_page : data.length,
        loading: false
      })
    } catch (error) {
      setApiReq({
        error,
        loading: false
      })
    }
  }

  useEffect(() => {
    if (url) {
      getDataFromApi(url)
    }
  }, [url])
  return apiReq
}

export function generateUrl(params) {
  let { mode, type, query, page } = params
  if (mode === 'search' && !query) {
    return false
  }

  switch (mode) {
    case 'top':
      return `https://api.jikan.moe/v3/${mode}/${type}/${page}`
    case 'search':
      return `https://api.jikan.moe/v3/${mode}/${type}?q=${query}&page=${page}`
    default:
      return false
  }
}
