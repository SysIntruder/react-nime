import React, { useState, useRef } from 'react'
import { Layout, Typography, Row, Col, Progress } from 'antd'

import InfoCard from './component/InfoCard'
import SearchBar from './component/SearchBar'
import Navigation from './component/Navigation'
import Paging from './component/Paging'

import { useFetch, generateUrl } from './methods'

import './App.css'

const { Header, Content, Footer } = Layout
const { Text, Title } = Typography

const categoryList = [
  {
    top: 'anime',
    search: 'anime'
  },
  {
    top: 'manga',
    search: 'manga'
  },
  {
    top: 'characters',
    search: 'character'
  },
  {
    top: 'people',
    search: 'people'
  }
]

export default () => {
  const [query, setQuery] = useState({
    mode: 'top',
    type: 'anime',
    query: null,
    page: 1
  })
  const url = generateUrl(query)
  const { data, total, loading, error } = useFetch(url)

  const pageType = useRef(query.type)

  const handleSearchSelect = (search) => {
    setQuery({
      ...search,
      page: 1
    })
  }

  const handleSearchData = (search) => {
    setQuery({
      ...search,
      page: 1,
      mode: 'search'
    })
    pageType.current = search.type
  }

  const handleTopData = (type) => {
    setQuery({
      type,
      page: 1,
      mode: 'top'
    })
    pageType.current = type
  }

  const handlePagination = (page) => {
    setQuery({
      ...query,
      page
    })
  }

  return (
    <Layout>
      <Header className="site-header">
        <Row align="middle">
          <Col span={24} style={{ padding: '8px 0' }}>
            <Title level={1} style={{ color: '#5cdbd3', margin: 0 }}>
              React-Nime
            </Title>
            <Text style={{ color: '#5cdbd3' }}>
              React.js anime information powered with Jikan API
            </Text>
          </Col>
          <Col span={24} style={{ padding: '8px 0' }}>
            <SearchBar
              data={categoryList}
              onSelect={handleSearchSelect}
              onSearch={handleSearchData}
            />
          </Col>
          <Col span={24} style={{ padding: '8px 0' }}>
            <Navigation
              data={categoryList}
              onSelect={handleTopData}
              isActive={query.mode}
            />
          </Col>
        </Row>
      </Header>
      <Content className="site-content">
        <Paging total={total} onSelect={handlePagination} />
        <div>
          {loading ? (
            <div style={{ textAlign: 'center' }}>
              <Progress status="active" percent={100} showInfo={false} />
              <Text>Loading data</Text>
            </div>
          ) : error ? (
            <div style={{ textAlign: 'center' }}>
              <Text>An error occured</Text>
            </div>
          ) : (
            <InfoCard
              data={data}
              mode={query.mode}
              type={pageType.current}
            />
          )}
        </div>
      </Content>
      <Footer className="site-footer">React-Nime © 2020</Footer>
    </Layout>
  )
}
